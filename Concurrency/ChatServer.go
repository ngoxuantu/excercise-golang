package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
	"sync"
)

type Client struct {
	Name   string
	Conn   net.Conn
	Active bool
}

func main() {
	clients := make(map[string]Client)

	var mu sync.Mutex
	var messageChannel = make(chan string)
	go func() {
		for {
			message := <-messageChannel
			mu.Lock()
			for _, client := range clients {
				if client.Active {
					client.Conn.Write([]byte(message))
				}
			}
			mu.Unlock()
		}
	}()
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	fmt.Println("Chat server is running on port 8080")
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
			continue
		}
		go handleClient(conn, messageChannel, &clients, &mu)
	}
}

func handleClient(conn net.Conn, messageChannel chan string, clients *map[string]Client, mu *sync.Mutex) {
	defer conn.Close()

	conn.Write([]byte("Enter your name:\n"))
	reader := bufio.NewReader(conn)
	name, _ := reader.ReadString('\n')
	name = strings.TrimSpace(name)
	mu.Lock()
	(*clients)[name] = Client{name, conn, true}
	mu.Unlock()
	welcomeMessage := fmt.Sprintf("Welcome %s join the channel!\n", name)
	messageChannel <- welcomeMessage
	fmt.Println(welcomeMessage)

	for {
		message, err := reader.ReadString('\n')
		if err != nil {
			mu.Lock()
			delete(*clients, name)
			mu.Unlock()
			disconnectMessage := fmt.Sprintf("%s has left the chat.\n", name)
			fmt.Println(disconnectMessage)
			messageChannel <- disconnectMessage
			return
		}
		messageChannel <- fmt.Sprintf("[%s]: %s", name, message)
		fmt.Printf("[%s]: %s", name, message)
	}
}
