package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	tasks := []int{1, 2, 3, 4, 5, 6, 10}
	n := 3
	semaphore := make(chan struct{}, n)
	var wg sync.WaitGroup
	for i, task := range tasks {
		wg.Add(1)
		semaphore <- struct{}{}
		go worker(i+1, task, &wg, semaphore)
	}
	wg.Wait()
	fmt.Print("Tất cả job đã hoàn thành")
}

func worker(id int, task int, wg *sync.WaitGroup, semaphore chan struct{}) {
	defer wg.Done()
	defer func() { <-semaphore }()
	fmt.Printf("Go routine %d đang xử lý job %d\n", id, task)
	time.Sleep(1 * time.Second)
	fmt.Printf("Go routine %d đã xử lý xong job %d\n", id, task)
}
