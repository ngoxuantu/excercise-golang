package main

import "fmt"

func main() {
	arr := [10]int{19, 12, 12, 3, 6, 7, 8, 9, 10}
	sum1 := 0
	sum2 := 0
	cha := make(chan int, 2)
	for _, v := range arr[0:5] {
		sum1 += v
	}
	cha <- sum1
	for _, v := range arr[5:] {
		sum2 += v
	}
	cha <- sum2
	x, y := <-cha, <-cha
	fmt.Println(x + y)
}
