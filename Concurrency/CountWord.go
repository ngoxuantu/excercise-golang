package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"sync"
)

type Container struct {
	mu       sync.Mutex
	counters map[string]int
}

func (c *Container) inc(name string) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.counters[name]++
}

func main() {
	filesName := []string{"text1.txt", "text2.txt", "text3.txt"}
	c := Container{
		counters: map[string]int{},
	}
	for _, fileName := range filesName {
		file, _ := os.Open(fileName)
		fileScanner := bufio.NewScanner(file)

		for fileScanner.Scan() {
			line := fileScanner.Text()
			words := strings.Fields(line)
			for _, word := range words {
				c.inc(word)

			}
		}
		file.Close()
	}
	for key, value := range c.counters {
		fmt.Println(key, value)
	}
}
