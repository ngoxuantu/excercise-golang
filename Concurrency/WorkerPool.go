package main

import (
	"fmt"
)

func main() {
	jobs := make(chan int, 100)
	results := make(chan int, 100)
	for w := 1; w <= 3; w++ {
		go worker(w, jobs, results)
	}
	count := 0
	for j := 1; j <= 200; j++ {
		jobs <- j
	}
	for a := 1; a <= 200; a++ {
		<-results
	}
	fmt.Println(count)
}

func worker(i int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		fmt.Println("worker", i, "start", j)
		results <- j * j
	}
}
