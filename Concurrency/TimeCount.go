package main

import (
	"fmt"
	"time"
)

func main() {
	tick := time.Tick(1 * time.Second)
	done := make(chan bool)
	go func() {
		for i := 0; i < 16; i++ {
			select {
			case <-tick:
				fmt.Println("Count :", i)
			}
		}
		done <- true
	}()

	select {
	case <-done:
		fmt.Println("Finished counting")
	case <-time.After(time.Second * 12):
		fmt.Println("Timed out")
	}

}
