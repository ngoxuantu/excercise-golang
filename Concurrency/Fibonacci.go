package main

import (
	"fmt"
	"sync"
)

func fibonacci(n int) int {
	if n <= 1 {
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}

func worker(ch chan<- [2]int, i int, wg *sync.WaitGroup) {
	defer wg.Done()
	ch <- [2]int{i, fibonacci(i)}
}

func main() {
	fmt.Println("Nhập số lượng số fibonacci: ")
	var n int
	fmt.Scan(&n)
	var wg sync.WaitGroup
	ch := make(chan [2]int, n)
	for i := 0; i < n; i++ {
		wg.Add(1)
		go worker(ch, i, &wg)
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	results := make(map[int]int)
	for result := range ch {
		results[result[0]] = result[1]
	}
	for i := 0; i < n; i++ {
		fmt.Printf("Fibonacci(%d) = %d\n", i, results[i])
	}

}
