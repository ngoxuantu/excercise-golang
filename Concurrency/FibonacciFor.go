package main

import "fmt"

func main() {
	var n int
	fmt.Println("Nhập số lượng số fibonacci:")
	fmt.Scan(&n)
	result := make(map[int]int)
	x, y := 0, 1
	for i := 0; i < n; i++ {
		result[i] = x
		x, y = y, x+y
	}
	fmt.Println(result)
}
