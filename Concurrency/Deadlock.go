package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
	}()
	go func() {
		for value := range ch {
			fmt.Println(value) // Đọc dữ liệu từ channel
		}
	}()

	time.Sleep(time.Second)
}
