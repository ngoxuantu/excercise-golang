package main

import (
	"fmt"
	"time"
)

func main() {
	go func() {
		for i := 0; i < 100; i++ {
			if i%2 == 0 {
				fmt.Println(i)
			}
		}
	}()

	go func() {
		for i := 0; i < 100; i++ {
			if i%2 != 0 {
				fmt.Println(i)
			}
		}
	}()
	time.Sleep(2 * time.Second)
}
