package main

import (
	"context"
	"fmt"
	"time"
)

func longTask(ctx context.Context) {
	select {
	case <-time.After(5 * time.Second):
		fmt.Println("Công việc đang được tiến hành")
	case <-ctx.Done():
		fmt.Println("Công việc bị hủy", ctx.Err())
	}
}

func main() {
	fmt.Println("Bắt đầu công việc")
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	go longTask(ctx)
	time.Sleep(6 * time.Second)
	fmt.Println("Kết thúc chương trình")
}
