package main

import (
	"fmt"
	"sync"
)

func main() {
	numbers := []int{1, 2, 3, 5, 65, 45, 523, 2, 4, 46, 56, 23, 12, 2}
	evenChan := make(chan int)
	evenDouble := make(chan int)
	result := 0
	var wg sync.WaitGroup
	go filterEven(numbers, evenChan)
	go filterDouble(evenChan, evenDouble)
	wg.Add(1)
	go sum(evenDouble, &wg, &result)
	wg.Wait()
	fmt.Println("Tổng kết quả là:", result)
}

func filterEven(numbers []int, ch chan<- int) {
	defer close(ch)
	for i := 0; i < len(numbers); i++ {
		if numbers[i]%2 == 0 {
			ch <- numbers[i]
		}
	}
}

func filterDouble(in <-chan int, ch chan<- int) {
	defer close(ch)
	for i := range in {
		ch <- i * 2
	}

}

func sum(ch <-chan int, wg *sync.WaitGroup, result *int) {
	defer wg.Done()
	for number := range ch {
		*result += number
	}
}
