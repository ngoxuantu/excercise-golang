package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handler(ctx context.Context, s3Event events.S3Event) {
	for _, record := range s3Event.Records {
		s3 := record.S3
		fmt.Printf("[%s - %s] Event Source: %s, Event Name: %s\n",
			record.EventTime, record.EventName, s3.Bucket.Name, s3.Object.Key)
	}
}

func main() {
	lambda.Start(handler)
}
