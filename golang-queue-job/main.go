package main

import (
	"fmt"
	"time"
)

type Job interface {
	Process() bool
}

type Worker struct {
	WorkerId   int
	Done       chan bool
	JobRunning chan Job
}

func NewWorker(workId int, jobChan chan Job) *Worker {
	return &Worker{
		WorkerId:   workId,
		Done:       make(chan bool),
		JobRunning: jobChan,
	}
}

func (w *Worker) Run() {
	go func() {
		for {
			select {
			case job := <-w.JobRunning:
				fmt.Println("Job running", w.WorkerId)
				job.Process()
			case <-w.Done:
				fmt.Println("Stop worker", w.WorkerId)
			}
		}
	}()
}

func (w *Worker) Stop() {
	fmt.Println("Stop worker id: ", w.WorkerId)
	w.Done <- true
}

type JobQueue struct {
	Workers    []*Worker
	JobRunning chan Job
	Done       chan bool
}

func NewJobQueue(numOfWorker int) JobQueue {
	workers := make([]*Worker, numOfWorker)
	jobRunning := make(chan Job)

	for i := 0; i < numOfWorker; i++ {
		workers[i] = NewWorker(i, jobRunning)
	}

	return JobQueue{
		Workers:    workers,
		JobRunning: jobRunning,
		Done:       make(chan bool),
	}
}

func (jq *JobQueue) Push(job Job) {
	jq.JobRunning <- job
}

func (jq JobQueue) Start() {
	go func() {
		for i := 0; i < len(jq.Workers); i++ {
			jq.Workers[i].Run()
		}
	}()

	go func() {
		for {
			select {
			case <-jq.Done:
				for i := 0; i < len(jq.Workers); i++ {
					jq.Workers[i].Stop()
				}
				return
			}
		}
	}()

}

func (jq JobQueue) Stop() {
	jq.Done <- true
}

type Sender struct {
	Email string
}

func (s Sender) Process() bool {
	if s.Email == "d@gmail.com" {
		return false
	}
	fmt.Println(s.Email)

	return true
}

func main() {
	emails := []string{
		"a@gmail.com",
		"b@gmail.com",
		"c@gmail.com",
		"d@gmail.com",
		"e@gmail.com",
	}

	jobQueue := NewJobQueue(4)
	jobQueue.Start()
	for _, email := range emails {
		sender := Sender{Email: email}
		jobQueue.Push(sender)
	}

	time.Sleep(time.Second * 3)

}
