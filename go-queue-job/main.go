package main

import (
	"fmt"
	"time"
)

type Job interface {
	Process()
}

type Worker struct {
	WorkerId   int
	Done       chan bool
	JobRunning chan Job
}

func NewWorker(workerId int, jobRunning chan Job) *Worker {
	return &Worker{
		WorkerId:   workerId,
		Done:       make(chan bool),
		JobRunning: jobRunning,
	}
}

func (w *Worker) Run() {
	fmt.Println("Running worker: ", w.WorkerId)
	go func() {
		for {
			select {
			case job := <-w.JobRunning:
				fmt.Println("Worker running: ", w.WorkerId)
				job.Process()
			case <-w.Done:
				fmt.Println("Worker stop: ", w.WorkerId)
				return
			}
		}
	}()
}

func (w *Worker) StopWorker() {
	w.Done <- true
}

type JobQueue struct {
	Workers    []*Worker
	JobRunning chan Job
	Done       chan bool
}

func NewJobQueue(numberOfWorkers int) JobQueue {
	workers := make([]*Worker, numberOfWorkers)
	jobRunning := make(chan Job)

	for i := 0; i < numberOfWorkers; i++ {
		workers[i] = NewWorker(i, jobRunning)
	}
	return JobQueue{
		Workers:    workers,
		JobRunning: jobRunning,
		Done:       make(chan bool),
	}
}

func (jq *JobQueue) Push(job Job) {
	jq.JobRunning <- job
}

func (jq *JobQueue) Stop() {
	jq.Done <- true
}

func (jq *JobQueue) Start() {
	go func() {
		for i := 0; i < len(jq.Workers); i++ {
			jq.Workers[i].Run()
		}

	}()

	go func() {
		for {
			select {
			case jq.Done <- true:
				for i := 0; i < len(jq.Workers); i++ {
					jq.Workers[i].StopWorker()
				}
			}
		}
	}()

}

type Sender struct {
	Email string
}

func (s Sender) Process() {
	fmt.Println("Sender Process: " + s.Email)
}
func main() {
	emails := []string{
		"a@gmail.com",
		"b@gmail.com",
		"c@gmail.com",
		"d@gmail.com",
		"e@gmail.com",
	}

	jobQueue := NewJobQueue(100)
	fmt.Println(jobQueue)
	jobQueue.Start()
	for _, email := range emails {
		sender := Sender{email}
		jobQueue.Push(sender)
	}

	time.AfterFunc(time.Second*2, func() {
		jobQueue.Stop()
	})
	time.Sleep(time.Second * 6)

}
