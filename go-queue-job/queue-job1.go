package main

import (
	"fmt"
	"time"
)

type Job interface {
	Process()
}

type Worker struct {
	WorkerId   int
	JobRunning chan Job
	Done       chan bool
}

func NewWorker(WorkerId int, JobRunning chan Job) *Worker {
	return &Worker{
		WorkerId:   WorkerId,
		JobRunning: JobRunning,
		Done:       make(chan bool),
	}
}

func (worker *Worker) Run() {
	fmt.Println("Worker Running", worker.WorkerId)
	go func() {
		for {
			select {
			case job := <-worker.JobRunning:
				fmt.Println("Job Running", job)
				job.Process()
			case <-worker.Done:
				fmt.Println("Worker done", worker.WorkerId)
				return

			}
		}
	}()

}

func (worker *Worker) StopWorker() {
	worker.Done <- true
}

type JobQueue struct {
	Workers    []*Worker
	JobRunning chan Job
	Done       chan bool
}

func NewJobQueue(numberOfWorkers int) JobQueue {
	workers := make([]*Worker, numberOfWorkers)
	jobRunning := make(chan Job)

	for i := 0; i < numberOfWorkers; i++ {
		workers[i] = NewWorker(i, jobRunning)
	}
	return JobQueue{
		Workers:    workers,
		JobRunning: jobRunning,
		Done:       make(chan bool),
	}
}

func (jq *JobQueue) Push(job Job) {
	jq.JobRunning <- job
}

func (jq *JobQueue) Stop() {
	jq.Done <- true
}

func (jq *JobQueue) Start() {
	go func() {
		for _, worker := range jq.Workers {
			worker.Run()
		}
	}()

	go func() {
		for {
			select {
			case <-jq.Done:
				for _, worker := range jq.Workers {
					go worker.StopWorker()
				}
			}
		}
	}()
}

type Sender struct {
	Email string
}

func (sender *Sender) Process() {
	fmt.Println(sender.Email)
}

func main() {
	emails := []string{
		"a@example.com",
		"b@example.com",
		"v@example.com",
		"d@example.com",
		"e@example.com",
	}
	numberOfWorkers := 100
	jobQueue := NewJobQueue(numberOfWorkers)
	jobQueue.Start()
	for _, email := range emails {
		sender := &Sender{email}
		jobQueue.Push(sender)
	}

	time.AfterFunc(time.Second*2, func() {
		jobQueue.Stop()
	})

	time.Sleep(time.Second * 3)
}
