package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(pow2(3, 2, 10), pow2(3, 4, 20))
}

func pow2(x, n, lim float64) float64 {
	if v := math.Pow(x, n); v < lim {
		fmt.Printf("%g < %g \n", v, lim)
	} else {
		fmt.Printf("%g > %g \n", v, lim)
	}
	return lim
}
