package main

import "fmt"

type Vertex struct {
	X int
	Y int
	C int
}

func main() {
	v := Vertex{1, 2, 4}
	p := &v
	p.X = 1e9
	fmt.Println(v)
}
