package main

import (
	"fmt"
	"strconv"
)

func fibonacci() func() int {
	a, b := 0, 1
	return func() int {
		result := a
		a, b = b, a+b
		return result
	}
}
func main() {
	var n int
	sum := 0
	fmt.Print("Enter a number: ")
	fmt.Scan(&n)
	f := fibonacci()
	for i := 0; i <= n; i++ {
		sum += f()
	}
	numberString := strconv.Itoa(sum)
	if len(numberString) > 6 {
		lastSixDigits := numberString[len(numberString)-6:]
		fmt.Println("Last six digits:", lastSixDigits)
	} else {
		fmt.Println("Last six digits:", sum)
	}

}
