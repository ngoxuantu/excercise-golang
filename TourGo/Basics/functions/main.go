package main

import "fmt"

func main() {
	fmt.Println(add(10, 30))
}

func add(x, y int) int {
	return x + y
}
