package main

import "fmt"

type Vertex struct {
	Lat, Long float64
}

var m = map[string]Vertex{
	"Bell Labs": {
		40.21212, 34.43434,
	},
	"Google ": {
		324.21212, 545454.323,
	},
}

func main() {
	fmt.Println(m["Google "])
}
