package main

import (
	"fmt"
)

func main() {
	var s = "hello world"
	// lấy phần tử từ index 0 tới 4
	//hello := s[:5]
	// lấy phần tử từ index 6 tới hết
	//world := s[6:]
	// có thể thao tác trực tiếp
	//s1 := "hello world"[:5]
	//s2 := "hello world"[6:]

	fmt.Println("len(s): ", len(s))

}
