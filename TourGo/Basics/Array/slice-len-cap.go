package main

import "fmt"

func main() {
	s := []int{2, 3, 4, 2, 4, 5}
	printSlice(s)
}

func printSlice(s []int) {
	fmt.Printf("len = %d cap =%d, %v\n", len(s), cap(s), s)
}
