package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
)

func Pic(dx, dy int) [][]uint8 {
	var x = make([][]uint8, dy)
	for i := 0; i < dx; i++ {
		s := make([]uint8, dx)
		for j := 0; j < dy; j++ {
			s[j] = uint8((i + j) / 2)

		}
		x[i] = s
	}
	fmt.Println(x)
	return x

}

func main() {
	dx, dy := 256, 256
	pic := Pic(dx, dy)
	println(pic)
	img := image.NewGray(image.Rect(0, 0, dx, dy))

	for y := 0; y < dy; y++ {
		for x := 0; x < dx; x++ {
			img.SetGray(x, y, color.Gray{Y: pic[x][y]})
		}
	}
	file, err := os.Create("output.png")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	err = png.Encode(file, img)
	if err != nil {
		panic(err)
	}
}
