package main

import "fmt"

func main() {
	var n int
	sum := 0
	fmt.Print("Enter a number: ")
	fmt.Scan(&n)
	for i := 1; i <= n; i++ {
		sum += i * i
	}
	fmt.Println("Your number is:", sum)
}
