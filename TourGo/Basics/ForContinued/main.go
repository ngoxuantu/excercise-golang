package main

func main() {
	sum := 1
	for sum < 100 {
		println(sum)
		sum += sum
	}
	println(sum)
}
