package main

import (
	"fmt"
	"math"
)

func main() {
	for x := 1.0; x < 5.0; x++ {
		result := Sqrt(x)
		fmt.Printf("Sqrt %v = %v (math.sqrt = %v)\n", x, result, math.Sqrt(x))
	}
}

func Sqrt(x float64) float64 {
	z := 1.0
	for i := 0; i < 10; i++ {
		z -= (z*z - x) / (2 * z)
		fmt.Printf("Iterations %v: z = %v \n", i+1, z)
	}
	return z
}
