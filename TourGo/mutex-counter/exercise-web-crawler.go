package main

import (
	"fmt"
	"strings"
	"sync"
)

type Fetcher interface {
	// Fetch returns the body of URL and
	// a slice of URLs found on that page.
	Fetch(url string) (body string, urls []string, err error)
}

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func Crawl(url string, depth int, fetcher Fetcher, c *SafeUrl) {
	// TODO: Fetch URLs in parallel.
	// TODO: Don't fetch the same URL twice.
	// This implementation doesn't do either:
	if depth <= 0 {
		return
	}
	_, urls, err := fetcher.Fetch(url)
	if err != nil {
		//fmt.Println(err)
		return
	}
	//fmt.Printf("found: %s %q\n", url, body)

	urlOld := c.Value("url")
	if !strings.Contains(urlOld, url) {
		urlSt := fmt.Sprintf("%s %s", urlOld, url)
		c.Inc("url", urlSt)
	}
	for _, u := range urls {
		Crawl(u, depth-1, fetcher, c)
	}
	return
}

func main() {
	c := SafeUrl{v: make(map[string]string)}
	Crawl("https://golang.org/", 4, fetcher, &c)
	fmt.Println(c.Value("url"))

}

// fakeFetcher is Fetcher that returns canned results.
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
	body string
	urls []string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}
	return "", nil, fmt.Errorf("not found: %s", url)
}

// fetcher is a populated fakeFetcher.
var fetcher = fakeFetcher{
	"https://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"https://golang.org/pkg/",
			"https://golang.org/cmd/",
		},
	},
	"https://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"https://golang.org/",
			"https://golang.org/cmd/",
			"https://golang.org/pkg/fmt/",
			"https://golang.org/pkg/os/",
		},
	},
	"https://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
	"https://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
}

type SafeUrl struct {
	mu sync.Mutex
	v  map[string]string
}

func (c *SafeUrl) Inc(key string, url string) {
	c.mu.Lock()
	c.v[key] = url
	c.mu.Unlock()
}

func (c *SafeUrl) Value(key string) string {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.v[key]
}
