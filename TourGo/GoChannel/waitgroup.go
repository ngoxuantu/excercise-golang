package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg sync.WaitGroup

func g1() {
	fmt.Println("g1")
	wg.Done()
}

func g2() {
	fmt.Println("g2")

	wg.Done()

}

func main() {
	fmt.Println("Begin")
	go g1()
	go g2()
	fmt.Println(runtime.NumGoroutine())

	wg.Wait()
	fmt.Println("Done")
}
