package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (rot rot13Reader) Read(p []byte) (n int, err error) {
	n, err = rot.r.Read(p)
	for i := 0; i < len(p); i++ {
		if p[i] >= 'A' && p[i] <= 'z' {
			p[i] += 13
			if p[i] > 'z' {
				p[i] -= 26
			}
		}
	}
	return
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	written, err := io.Copy(os.Stdout, &r)
	if err != nil {
		fmt.Printf("error listening for server: %s\n", err)
	}

	fmt.Println(written)
}
