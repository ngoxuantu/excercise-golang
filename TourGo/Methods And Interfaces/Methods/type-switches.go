package main

import (
	"fmt"
	"go/types"
)

type Employee struct {
	firstName string
	lastName  string
	age       int
}

func do(i interface{}) {
	switch v := i.(type) {
	case int:
		fmt.Printf("Twice %v is %v\n", v, v*2)
	case string:
		fmt.Printf("%q is %v bytes long\n", v, len(v))
	case types.Array:
		fmt.Printf("It is array")
	case types.Object:
		fmt.Printf("Type is Opject")
	default:
		fmt.Printf("I don't know about type %T!\n", v)
	}
}

func main() {
	var user Employee
	a := []int{}
	do(21)
	do("hello")
	do(true)
	do(a)
	do(user)

}
