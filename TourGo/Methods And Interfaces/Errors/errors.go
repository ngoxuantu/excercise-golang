package main

import (
	"fmt"
	"math"
)

type ErrNegativeSqrt float64

type error interface {
	Error() string
}

func (e ErrNegativeSqrt) Error() string {
	return fmt.Sprintln(" is a negative number")
}

func Sqrt(x float64) (float64, error) {
	if x > 0 {
		return math.Sqrt(x), nil
	}
	err := ErrNegativeSqrt(x)
	return x, err
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}
