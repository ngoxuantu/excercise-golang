package main

import (
	"fmt"
	"strconv"
	"time"
)

type MyError struct {
	When time.Time
	What string
}

func (e *MyError) Error() string {
	return fmt.Sprintf("at %v, %s", e.When, e.What)
}

type error interface {
	Error() string
}

func run() error {
	return &MyError{
		time.Now(),
		"It didn't work",
	}
}

func main() {
	i, err := strconv.Atoi("-42")
	if err != nil {
		fmt.Printf("couldn't convert number: %v\n", err)
		return
	}
	fmt.Println("Converted integer:", i)

	if err := run(); err != nil {
		fmt.Println(err)
	}
}
