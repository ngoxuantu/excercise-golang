package main

import "fmt"

func main() {
	n := 5
	fmt.Printf("Fibonacci number at position %d is: %d\n", n, fibonacci(n))
}

func fibonacci(n int) []int {
	arr := make([]int, 0)

	if n <= 1 {
		arr = append(arr, 1)
	}
	x, y := 0, 1

	for i := 0; i <= n; i++ {
		x, y = y, x+y
		arr = append(arr, x)
	}
	return arr
}
