package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net"
	"net/http"
	"strconv"
	"time"
)

const keyServerAddr = "serverAddr"
const (
	dbDriver = "mysql"
	dbUser   = "root"
	dbPass   = "root"
	dbName   = "management_book"
)

func main() {
	// Thông tin kết nối đến MySQL
	db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/management_book")
	if err != nil {
		log.Fatal(err)
	}

	// Kiểm tra kết nối đến MySQL
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MySQL!")

	db.SetMaxOpenConns(1)
	err = createBookTable(db)
	createServer()
}
func createServer() {
	r := mux.NewRouter()
	r.HandleFunc("/books/create", postCreateBook).Methods("POST")
	r.HandleFunc("/books/update/{id}", updateBook).Methods("PATCH")
	r.HandleFunc("/books/get-all", getAllBook).Methods("GET")
	r.HandleFunc("/books/get-by-id/{id}", getByIdBook).Methods("GET")
	r.HandleFunc("/books/search", getSearchBook).Methods("GET")
	r.HandleFunc("/books/delete/{id}", deleteBook).Methods("DELETE")

	ctx := context.Background()
	server := &http.Server{
		Addr:    ":3333",
		Handler: r,
		BaseContext: func(listener net.Listener) context.Context {
			ctx = context.WithValue(ctx, keyServerAddr, listener.Addr().String())
			return ctx
		},
	}

	err := server.ListenAndServe()
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error listening for server: %s\n", err)
	}

}

func deleteBook(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	vars := mux.Vars(r)
	idStr := vars["id"]
	bookID, _ := strconv.Atoi(idStr)
	err = deleteBookQuery(db, bookID)
	if err != nil {
		log.Printf("Error %s when deleting product table", err)
	}
}

func deleteBookQuery(db *sql.DB, id int) error {
	query := "DELETE FROM books WHERE id = ?"
	_, err := db.Exec(query, id)
	if err != nil {
		return err
	}

	return nil
}
func getSearchBook(w http.ResponseWriter, r *http.Request) {
	db, _ := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	key := r.FormValue("key")
	books, _ := searchBookQuery(db, key)

	for _, book := range books {
		fmt.Printf("ID: %d, Title: %s, Price: %v\n", book.Id, book.Name, book.Price)
	}
}

func getByIdBook(w http.ResponseWriter, r *http.Request) {
	db, _ := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)

	vars := mux.Vars(r)
	idStr := vars["id"]
	bookID, _ := strconv.Atoi(idStr)
	book, _ := getByIdBookQuery(db, bookID)
	fmt.Printf("ID: %d, Title: %s, Price: %v\n", book.Id, book.Name, book.Price)

}

func getAllBook(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	books, err := getAllBookQuery(db)

	for _, book := range books {
		fmt.Printf("ID: %d, Title: %s, Price: %v\n", book.Id, book.Name, book.Price)
	}
	if err != nil {
		return
	}
}

func getAllBookQuery(db *sql.DB) ([]Book, error) {
	query := "SELECT id, name, price FROM books"
	rows, err := db.Query(query)

	if err != nil {
		log.Fatal("Error executing query:", err)
	}
	var books []Book

	for rows.Next() {
		var book Book
		err := rows.Scan(&book.Id, &book.Name, &book.Price)
		if err != nil {
			log.Fatal("Error scanning row:", err)
			return nil, err

		}
		books = append(books, book)
	}
	return books, nil
}
func createBookTable(db *sql.DB) error {
	query := "CREATE TABLE IF NOT EXISTS books (id int primary key auto_increment, name text,price int, created_at datetime default CURRENT_TIMESTAMP, updated_at datetime default CURRENT_TIMESTAMP)"
	ctx, cancelFunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelFunc()
	res, err := db.ExecContext(ctx, query)
	if err != nil {
		log.Printf("Error %s when creating product table", err)
		return err
	}
	rows, err := res.RowsAffected()
	if err != nil {
		log.Printf("Error %s when getting rows affected", err)
		return err
	}
	log.Printf("Rows affected when creating table: %d", rows)
	return nil
}

type Book struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Price int    `json:"price"`
}

func postCreateBook(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	ctx := r.Context()

	var book Book
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err = json.NewDecoder(r.Body).Decode(&book)
	fmt.Printf("%s: got /hello request\n", ctx.Value(keyServerAddr))
	err = CreateBook(db, book.Name, book.Price)
	if err != nil {
		return
	}
}

func CreateBook(db *sql.DB, name string, price int) error {
	query := "INSERT INTO books (name, price) values (?,?)"
	_, err := db.Exec(query, name, price)
	if err != nil {
		return err
	}
	return nil
}

func updateBook(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)

	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	vars := mux.Vars(r)
	idStr := vars["id"]

	var book Book
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err = json.NewDecoder(r.Body).Decode(&book)

	bookID, err := strconv.Atoi(idStr)
	err = updateBookQuery(db, bookID, book.Name, book.Price)

	if err != nil {
		return
	}

	fmt.Println("Update book success")
	return
}

func updateBookQuery(db *sql.DB, id int, name string, price int) error {

	book, err := getByIdBookQuery(db, id)

	if book != nil {
		query := "UPDATE books SET name = ?, price = ? WHERE id = ?"
		_, err := db.Exec(query, name, price, id)
		if err != nil {
			return err
		}
	}

	if err != nil {
		return err
	}
	return err
}

func getByIdBookQuery(db *sql.DB, id int) (*Book, error) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	query := "SELECT id, name, price FROM books where id = ?"
	row := db.QueryRow(query, id)

	book := &Book{}
	err = row.Scan(&book.Id, &book.Name, &book.Price)
	if err != nil {
		return nil, err
	}
	return book, nil
}

func searchBookQuery(db *sql.DB, key string) ([]Book, error) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	fullNameQuery := "%" + key + "%"
	query := "SELECT id, name, price FROM books where name like ? "
	rows, _ := db.Query(query, fullNameQuery)
	var books []Book
	for rows.Next() {
		var book Book
		err = rows.Scan(&book.Id, &book.Name, &book.Price)
		books = append(books, book)
	}

	if err != nil {
		return nil, err
	}
	return books, nil
}
