package main

import (
	"fmt"
	"time"
)

func Producer(factor int, out chan<- int) {
	for i := 0; ; i++ {
		out <- i * factor
	}
}

// consumer: liên tục lấy các số từ channel ra để print
func Consumer(in <-chan int) {
	for v := range in {
		fmt.Println(v)
	}
}
func main() {
	// hàng đợi
	ch := make(chan int, 64)

	// tạo một chuỗi số với bội số 3
	go Producer(3, ch)

	// tạo một chuỗi số với bội số 5
	go Producer(5, ch)

	// tạo consumer
	go Consumer(ch)

	// thoát ra sau khi chạy trong một khoảng thời gian nhất định
	time.Sleep(5 * time.Second)
}
