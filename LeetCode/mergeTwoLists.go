package main

import (
	"fmt"
)

func main() {
	var list1 Node
	AddNodeAtEnd(&list1, 1)
	AddNodeAtEnd(&list1, 2)
	AddNodeAtEnd(&list1, 3)
	var list2 Node
	AddNodeAtEnd(&list2, 2)
	AddNodeAtEnd(&list2, 3)
	AddNodeAtEnd(&list2, 4)
	AddNodeAtEnd(&list2, 5)
	listMerge := mergeTwoLists(list1.Next, list2.Next)
	viewList(listMerge)
}

func mergeTwoLists(list1, list2 *Node) *Node {
	listMerge := new(Node)
	current := listMerge
	for list1 != nil && list2 != nil {
		if list1.Val < list2.Val {
			current.Next = list1
			list1 = list1.Next
		} else {
			current.Next = list2
			list2 = list2.Next
		}
		current = current.Next
	}
	if list1 != nil {
		current.Next = list1
	} else {
		current.Next = list2
	}
	return listMerge
}

type Node struct {
	Val  int
	Next *Node
}

func AddNodeAtEnd(head *Node, data int) *Node {
	newNode := &Node{
		Val:  data,
		Next: nil,
	}

	if head == nil {
		return newNode
	}

	currentNode := head
	for currentNode.Next != nil {
		currentNode = currentNode.Next
	}
	currentNode.Next = newNode

	return head
}

func viewList(listNode *Node) {
	for {
		fmt.Println(listNode.Val)
		if listNode.Next != nil {
			listNode = listNode.Next
		} else {
			break
		}
	}
}
