package main

import (
	"fmt"
)

func main() {
	strs := []string{"aca", "cba"}
	fmt.Println(longestCommonPrefix(strs))
}

func longestCommonPrefix(strs []string) string {
	result := ""
	tmpl := ""
	i := 0
	str := strs[0]
	for i = 0; i < len(str); i++ {
		tmpl += string(str[i])
		check := 0
		for _, v := range strs {
			if i >= len(v) || v[i] != str[i] {
				if i == 0 {
					return ""
				}
				continue
			} else {
				check = check + 1
			}
		}
		if check == len(strs) && len(tmpl) > len(result) {
			result = tmpl
		} else {
			tmpl = ""
		}
	}

	return result
}

func longestCommonPrefixOther(strs []string) string {
	for i := 0; ; i++ {
		for _, str := range strs {
			if i == len(str) || str[i] != strs[0][i] {
				return strs[0][:i]
			}
		}
	}
}
