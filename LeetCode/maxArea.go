package main

import "fmt"

func main() {
	height := []int{1, 8, 6, 2, 5, 4, 8, 3, 7}
	fmt.Println(maxArea(height))
	//maxArea(height)
}

func maxArea(height []int) int {
	maxLength := 0
	i := 0
	j := len(height) - 1

	for i < j {
		var minHeight int
		minHeight = min(height[i], height[j])
		maxLength = max(maxLength, minHeight*(j-i))
		if height[i] < height[j] {
			i++
		} else {
			j--
		}

	}

	return maxLength
}
