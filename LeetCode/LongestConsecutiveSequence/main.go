package main

import (
	"fmt"
	"slices"
)

func main() {
	nums := []int{1, 2, 0, 1}
	fmt.Println(longestConsecutive(nums))
}

func longestConsecutive(nums []int) int {
	maxArr := []int{}
	slices.Sort(nums)
	currentArr := []int{}
	for _, num := range nums {
		if len(currentArr) == 0 || num == currentArr[len(currentArr)-1]+1 {
			currentArr = append(currentArr, num)
		} else if num > currentArr[len(currentArr)-1] {
			currentArr = []int{num}
		} else if IntInSlice(num, currentArr) {
			continue
		}
		if len(currentArr) > len(maxArr) {
			maxArr = currentArr
		}
	}
	return len(maxArr)
}

func IntInSlice(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
