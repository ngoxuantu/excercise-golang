package main

import "fmt"

func main() {
	s := "babad"
	fmt.Println(longestPalindrome(s))
}

func longestPalindrome(s string) string {
	if len(s) == 0 {
		return ""
	}

	longest := ""

	for i := 0; i < len(s); i++ {
		maxLenOne := explainAroundCenter(s, i, i)
		if len(longest) < len(maxLenOne) {
			longest = maxLenOne
		}

		maxLenTwo := explainAroundCenter(s, i, i+1)
		if len(longest) < len(maxLenTwo) {
			longest = maxLenTwo
		}
	}

	return longest
}

func explainAroundCenter(s string, left, right int) string {
	for left >= 0 && right < len(s) && s[left] == s[right] {
		left--
		right++
	}
	return s[left+1 : right]
}
