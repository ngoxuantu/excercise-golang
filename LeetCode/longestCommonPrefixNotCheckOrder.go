package main

import (
	"fmt"
	"strings"
)

func main() {
	strs := []string{"reflower", "flow", "flight"}
	fmt.Println(longestCommonPrefixNotCheckOrder(strs))
}

func longestCommonPrefixNotCheckOrder(strs []string) string {
	result := ""
	tmpl := ""
	i := 0
	str := strs[0]
	for i = 0; i < len(str); i++ {
		tmpl += string(str[i])
		fmt.Println(string(str[i]))

		check := 0
		for _, v := range strs {

			if !strings.Contains(v, tmpl) {

			} else {
				check = check + 1
			}
		}
		if check == len(strs) && len(tmpl) > len(result) {
			result = tmpl
		} else {
			tmpl = ""
		}
	}

	return result
}
