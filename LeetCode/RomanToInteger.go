package main

import (
	"fmt"
)

func main() {
	s := "MCMXCIV"
	fmt.Println(romanToInt(s))
}

func romanToInt(s string) int {
	sum := 0

	for i := 0; i < len(s); i++ {
		tmp := getValue(string(s[i]))
		tmpNext := 0
		if i < len(s)-1 {
			tmpNext = getValue(string(s[i+1]))
		} else {
			tmpNext = 0
		}
		if tmpNext > tmp {
			tmp = -tmp
		}
		fmt.Println(tmp)
		sum += tmp
	}

	return sum
}

func getValue(s string) int {
	switch s {
	case "I":
		return 1
	case "V":
		return 5
	case "X":
		return 10
	case "L":
		return 50
	case "C":
		return 100
	case "D":
		return 500
	case "M":
		return 1000

	}
	return 0
}
