package main

import (
	"fmt"
	"regexp"
	"strings"
)

func main() {
	s := "A man, a plan, a canal: Panama"
	fmt.Println(isPalindrome(strings.ToLower(s)))
}

func isPalindrome(s string) bool {
	re := regexp.MustCompile(`[a-z0-9A-Z]+`)
	matches := re.FindAllString(s, -1)
	str := strings.Join(matches, "")
	str = strings.ToLower(str)
	fmt.Println(str)
	l := 0
	r := len(str) - 1
	for l <= r {
		if str[l] != str[r] {
			return false
		}
		l++
		r--
	}
	return true
}
