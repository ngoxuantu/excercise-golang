package main

import (
	"fmt"
)

func main() {
	a := 52
	convertToTitle(a)
}

func convertToTitle(columnNumber int) string {
	str := []rune{}
	integerPart := columnNumber
	for {
		if integerPart > 26 {
			remainder := integerPart % 26
			str = append([]rune{rune(remainder + 64)}, str...)
			integerPart = integerPart / 26
		} else {
			str = append([]rune{rune(integerPart + 64)}, str...)
			break
		}

	}
	fmt.Println('Z')
	fmt.Println(string(str))
	return string(str)
}
