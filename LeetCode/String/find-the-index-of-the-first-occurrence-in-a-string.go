package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println("Hello, playground")
	strStr("sadbutsad", "sad")
}

func strStr(haystack string, needle string) int {
	result := strings.Index(haystack, needle)
	return result
}
