package main

import "fmt"

func main() {
	s := "()"
	fmt.Println(validParentheses(s))
}

func validParentheses(s string) bool {
	stack := []rune{}
	mapping := map[rune]rune{
		'}': '{',
		']': '[',
		')': '(',
	}

	for _, v := range s {
		if v == '(' || v == '[' || v == '{' {
			stack = append(stack, v)
		} else if v == ')' || v == ']' || v == '}' {
			if len(stack) == 0 || stack[len(stack)-1] != mapping[v] {
				return false
			}
			stack = stack[:len(stack)-1]
		}
	}
	return len(stack) == 0
}
