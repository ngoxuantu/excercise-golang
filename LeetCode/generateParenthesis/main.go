package main

func main() {
	n := 3
	generateParenthesis(n)
}

func generateParenthesis(n int) []string {
	var result []string
	var batchTrack func(current string, open, close int)
	batchTrack = func(current string, open, close int) {
		if len(current) == n*2 {
			result = append(result, current)
			return
		}

		if open < n {
			batchTrack(current+"(", open+1, close)
		}
		if close < open {
			batchTrack(current+")", open, close+1)
		}

	}
	batchTrack("", 0, 0)
	return result
}
