package main

import (
	"fmt"
	"strconv"
)

func main() {
	x := 10013
	fmt.Println(isPalindrome(x))
}

func isPalindrome(x int) bool {
	if x < 0 {
		return false
	}
	l := 0
	r := len(strconv.Itoa(x)) - 1
	for l <= r || l <= r-1 {
		if strconv.Itoa(x)[r] != strconv.Itoa(x)[l] {
			return false
		}
		l++
		r--
	}

	return true
}
