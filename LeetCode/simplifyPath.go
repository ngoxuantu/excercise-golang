package main

import (
	"fmt"
	"strings"
)

func main() {
	path := "/a/./b/../../c/"
	fmt.Println(simplifyPath(path))
}

func simplifyPath(path string) string {
	var newPath []string
	strs := strings.Split(path, "/")

	for i := 0; i < len(strs); i++ {

		if strs[i] == "" {
			continue
		} else if strs[i] == "." {
			continue
		} else if strs[i] == ".." {
			if len(newPath)-1 >= 0 {
				newPath = newPath[:len(newPath)-1]
			}
			continue
		} else {
			newPath = append(newPath, strs[i])
		}
	}
	return "/" + strings.Join(newPath, "/")
}
