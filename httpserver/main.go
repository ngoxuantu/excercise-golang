package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
)

const keyServerAddr = "serverAddr"

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", getRoot)
	mux.HandleFunc("/hello", getHello)

	ctx := context.Background()
	server := &http.Server{
		Addr:    ":3333",
		Handler: mux,
		BaseContext: func(listener net.Listener) context.Context {
			ctx = context.WithValue(ctx, keyServerAddr, listener.Addr().String())
			return ctx
		},
	}

	err := server.ListenAndServe()
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error listening for server: %s\n", err)
	}

}

func getRoot(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		io.WriteString(w, "This is my website with only get method\n")
	case http.MethodPost:
		io.WriteString(w, "This is my website with only post method\n")
	case http.MethodPut:
		io.WriteString(w, "This is my website with only put method\n")
	case http.MethodPatch:
		io.WriteString(w, "This is my website with only patch method\n")
	case http.MethodDelete:
		io.WriteString(w, "This is my website with only delete method\n")

	}
}

func getHello(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	fmt.Printf("%s: got /hello request\n", ctx.Value(keyServerAddr))
	myName := r.PostFormValue("myName")
	if myName == "" {
		w.Header().Set("x-missing-fied", "myName")
		w.WriteHeader(http.StatusBadRequest)
	}
	io.WriteString(w, fmt.Sprintf("Hello, %s!\n", myName))
}
