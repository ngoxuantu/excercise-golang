package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net"
	"net/http"
)

const keyServerAddr = "serverAddr"

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/upload-file-slack", uploadFileSlack)

	ctx := context.Background()
	server := &http.Server{
		Addr:    ":3333",
		Handler: mux,
		BaseContext: func(listener net.Listener) context.Context {
			ctx = context.WithValue(ctx, keyServerAddr, listener.Addr().String())
			return ctx
		},
	}

	err := server.ListenAndServe()
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error listening for server: %s\n", err)
	}
}

func uploadFileSlack(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(10 << 20)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// Thêm các trường thông tin khác vào form data
	writer.WriteField("channels", "C06EZ1H9DJL")

	// Thêm file vào form data
	file, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, "Error retrieving file", http.StatusBadRequest)
		return
	}
	defer file.Close()

	fileField, err := writer.CreateFormFile("file", handler.Filename)
	if err != nil {
		fmt.Println("Error creating form file:", err)
		return
	}

	_, err = io.Copy(fileField, file)
	if err != nil {
		fmt.Println("Error copying file data:", err)
		return
	}

	// In ra nội dung body trước khi gửi request
	//fmt.Println("Request Body Before Sending:", body.String())

	// Kết thúc việc tạo form data
	writer.Close()

	// Tạo request với method, url và body là form data
	request, err := http.NewRequest("POST", "https://slack.com/api/files.upload", body)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}

	// Đặt header để xác định định dạng của dữ liệu là form data
	request.Header.Set("Content-Type", writer.FormDataContentType())
	token := "xoxb-3443947997460-6486199984643-QLeaW88CgDShp7jG9pfv6IHy"
	request.Header.Set("Authorization", "Bearer "+token)
	// Tạo một đối tượng `Client` và thực hiện request
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		fmt.Println("Error making request:", err)
		return
	}
	defer response.Body.Close()

	// Xử lý kết quả ở đây
	fmt.Printf("Status Code:%s", response.Body)

}
